>**Mateo 10:8** Sanad enfermos, limpiad leprosos, resucitad muertos, echad fuera demonios; de gracia recibisteis, dad de gracia.

El objetivo principal de este sitio es `poder compartir lo que todos estos años de alguna manera he podido obtener`. Así que espero que les sea de ayuda lo que encuentren en este sitio. Paz, vida y gozo a su corazón y familia. :gift:

Acá :point_down: podrán ver por categoría (por así decirlo) lo que tengo para compartirles.