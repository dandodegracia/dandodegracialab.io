---
title: Eliminando logs en Salesforce a traves de codigo
subtitle: Usnado consola anonima
date: 2020-06-26
bigimg: [{src: "/img/path.jpg", desc: "Path"}]
---

En esos días cuando estas programando en Salesforce, y en el Developer Console te lanza un mensaje como el siguiente:

> Having an active trace flag triggers debug logging. You have 1,020 MB of the maximum 1,000 MB of debug logs. Before you can edit trace flags, delete some debug logs.

Viendo un par de soluciones para eliminación de logs me encontré con una solución que realiza una eliminación de logs que puede ser ejecutado desde la consola anónima de Salesforce y podemos personalizar el query para adaptarlo a lo que necesitamos y que sean eliminados los logs. (No sé si sea la más óptima pero es funcional, si encuentro mejores soluciones las compartiré).

Y este es el código:

```apex
// Selecciona los primeros 100 logs.
List <Apexlog> loglist = [SELECT Id FROM Apexlog LIMIT 100];

// Realiza una recorrido de los logs y realiza una eliminación
for(Apexlog al: loglist){
    Http h = new Http();
    HttpRequest req = new HttpRequest();
    req.setEndpoint(Url.getOrgDomainUrl().toExternalForm()
    + '/services/data/v47.0/sobjects/Apexlog/'+al.Id);
    req.setMethod('DELETE');
    req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
    HttpResponse res = h.send(req);
    System.debug(res.getStatusCode());
}

System.debug('loglist'+loglist);
```

Esta porción de código la tome de acá: [how-to-delete-all-debug-apex-logs-in-apex](https://salesforce.stackexchange.com/questions/239636/how-to-delete-all-debug-apex-logs-in-apex)

Esto lo podemos personalizar, por ejemplo, si queremos eliminar solo los 10 primeros logs más pesados podemos realizarlo de la siguiente manera:

```apex
// Selecciona los primeros 10 logs más grandes
List <Apexlog> loglist = [SELECT Id FROM Apexlog ORDER BY LogLength DESC LIMIT 10];

// Realiza una recorrido de los logs y realiza una eliminación
for(Apexlog al: loglist){
    Http h = new Http();
    HttpRequest req = new HttpRequest();
    req.setEndpoint(Url.getOrgDomainUrl().toExternalForm()
    + '/services/data/v47.0/sobjects/Apexlog/'+al.Id);
    req.setMethod('DELETE');
    req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
    HttpResponse res = h.send(req);
    System.debug(res.getStatusCode());
}

System.debug('loglist'+loglist);
```

Espero que te sea de utilidad.

> **Isaías 43:18** No os acordéis de las cosas pasadas, ni traigáis a memoria las cosas antiguas.
